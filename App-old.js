import React from '../../../Library/Caches/typescript/2.9/node_modules/@types/react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import picNono from './assets/nourreddine.jpeg'

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.defaultText}>Open !</Text>
        <Text style={[styles.defaultText, styles.selectedText]}>Changes.</Text>
        <Text style={[styles.defaultText]}>Shake.</Text>
        <ImageBackground style={styles.picture} source={picNono}>
          <Text>Nono</Text>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#DDD',
    alignItems: 'flex-end',
    justifyContent: 'space-around',
  },
  defaultText: {
    //flex: 1,
    fontSize: 22,
    textAlign: 'center',
    padding: 10,
    margin: 5,
    borderWidth: StyleSheet.hairlineWidth,
    color: 'black'
  },
  selectedText: {
    //flex: 3,
    alignSelf: 'flex-end',
    backgroundColor: 'yellow'
  },
  picture: {
    flexDirection: 'column',
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  }
});
