import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  ListView,
  ActivityIndicator,
  ImageView,
  Image,
  Dimensions,
  Text
} from 'react-native';


export default class App extends Component{

  constructor(){
    super()
    
    this.renderRow = this.renderRow.bind(this)
    
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const products = []

    this.state = {
      products: products,
      datasource: this.ds.cloneWithRows(products),
      fetching: false
    }
  }


  renderRow(product){
    return (
      <ImageView source={product} >
      </ImageView>
    )
  }

  render(){
    return (
      <ScrollView horizontal={true}>
        <ActivityIndicator 
          size="large" 
          style={styles.spinner}
          animating={this.state.fetching}/>
           {this.state.products.map(
          (uri,i) => (
            <Image style={styles.thumb} key={i} source={{ uri }}/>
          )
          )}
        {/* <ListView 
           dataSource={this.state.datasource}
          renderRow={this.renderRow}>
         </ListView>  */}
      </ScrollView>
    )
  }

  componentDidMount(){
    this.setState({fetching: true})
    fetch('https://hplussport.com/api/products.php')
    .then(response => 
      response.json()
    )
    .then(products => products.map(product => product.image))
    .then(productImages => {
      console.log(productImages)
      this.setState({
        fetching: false , 
        products : productImages,
        datasource: this.ds.cloneWithRows(productImages)
      })
    }
    )
    .catch(err => {
      this.setState({fetching: false})
      console.err('Error loading products')
    })
  }

}

const styles = StyleSheet.create(
  {
    container: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    spinner: {
      //position: 'absolute',
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width
    },
    thumb: {
      width: 375,
      resizeMode: 'cover'
    }
  }
)
