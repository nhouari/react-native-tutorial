import React, { Component } from 'react'
import { WebView, StyleSheet} from 'react-native'

export default class WebPage extends React.Component {
    
    static navigationOptions = {
        title: 'Info'
    }
    
    constructor(props){
        super(props)
    }
    
    render() {
        return (
            <WebView 
            style={styles.container}
            source={this.props.navigation.state.params}
            contentInset={{top: -650}}
        />
        )
    }
}
  

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})
