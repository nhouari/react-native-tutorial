import React from 'react';
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native'

const ColorButton = ({backgroundColor, onSelect=f=>f}) => (
    <TouchableHighlight
        style={styles.button}
        underlayColor='orange'
        onPress={() => onSelect(backgroundColor)}>
        <View style={styles.row}>
            <View style={[styles.sample, { backgroundColor }]} />
            <Text style={styles.text}>{backgroundColor}</Text>
        </View>
    </TouchableHighlight>
)

const styles = StyleSheet.create({
    button: {
      margin: 10,
      padding: 10,
      borderWidth: 1,
      borderRadius: 7,
      backgroundColor: 'white',
      alignSelf: 'stretch',
      backgroundColor: 'rgba(255,255,255,.8)'
    },
    row: {
      flexDirection: 'row',
      height: 40,
    },
    sample: {
      height: 20,
      width: 20,
      borderRadius: 10,
      backgroundColor: 'white'
    },
    text: {
      padding: 5
    }  
  });

  export default ColorButton;