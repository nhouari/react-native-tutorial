import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet
} from 'react-native'
import ColorTools from 'color'


export default class ColorInfo extends React.Component {

    static navigationOptions = {
        title: 'Color info'
    }

    constructor(props) {
        super(props)
    }

    render() {
        const color = ColorTools(this.props.navigation.state.params.color)

        return (
            <View style={[styles.container, {backgroundColor: color}]}>
                <Text style={[styles.text, {color: color.negate()}]}>{this.props.navigation.state.params.color}</Text>
                <Text style={[styles.text, {color: color.negate()}]}>{color.hex()}</Text>
                <Text style={[styles.text, {color: color.negate()}]}>{color.rgb().string()}</Text>
                <Text style={[styles.text, {color: color.negate()}]}>{color.hsl().string}</Text>
            </View> 
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        margin: 10
    }
})