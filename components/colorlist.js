import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  ListView,
  AsyncStorage
} from 'react-native';

import ColorButton from './colorbutton'
import ColorForm from './colorform'

export default class ColorList extends React.Component {

  static navigationOptions = {
    title: 'Available Colors'
  }

  constructor(props) {
    super(props)

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const availableColors = [];


    this.state = {
      backgroundColor: 'blue',
      availableColors,
      datasource: this.ds.cloneWithRows(availableColors)
    }
    this.changeColor = this.changeColor.bind(this)
    this.renderRow = this.renderRow.bind(this)
    this.newColor = this.newColor.bind(this)
  }

  componentDidMount() {
    AsyncStorage.getItem('@ColorsListStore:Colors',
      (err, data) => {
        if (err){
          console.error('Error loading colors')
        } else {
          const availableColors = JSON.parse(data);
          this.setState({
            availableColors,
            datasource: this.ds.cloneWithRows(availableColors)
          })
        }
      }
    )
  }

  saveColors(colors) {
    AsyncStorage.setItem(
      '@ColorsListStore:Colors',
      JSON.stringify(colors)
    )
  }

  changeColor(backgroundColor) {
    this.setState({ backgroundColor })
  }

  renderRow(color) {
    const { navigate } = this.props.navigation
    return (
      <ColorButton backgroundColor={color} onSelect={() => navigate('Details', {color})}></ColorButton> //onSelect={this.props.onColorSelected}
    )
  }

  newColor(color) {
    const availableColors = [
      ...this.state.availableColors,
      color
    ]
    this.setState({
      availableColors,
      datasource: this.ds.cloneWithRows(availableColors)
    })

    this.saveColors(availableColors)
    console.log('Selected color ' , color)
    this.props.onColorSelected(color)
  }

  render() {
    const { backgroundColor, datasource } = this.state
    // Extract the navigation from the navigation properties
    // injected by the StackNavigator
    
    return (

      <ListView style={[styles.container, { backgroundColor }]} dataSource={datasource}
        renderRow={this.renderRow}
        renderHeader={
          () => (
            <ColorForm onNewColor={this.newColor} navigation={this.props.navigation}/>
          )
        }>
      </ListView>
    )
  }
}

ColorList.defaultProps = {
  onColorSelected: f => f
}

ColorList.propTypes = {
  onColorSelected: PropTypes.func
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    marginBottom: 20
  },
  header: {
    fontSize: 30,
    backgroundColor: 'lightgrey',
    paddingTop: 20,
    padding: 10,
    textAlign: 'center'
  }
});
