import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {
    View,
    Text,
    StyleSheet,
    TextInput
} from 'react-native'

export default class ColorForm extends Component {

    constructor(props){
        super(props)
        this.state = {
            txtColor: ''
        }
        this.submit = this.submit.bind(this)
    }

    submit(){
        this.props.onNewColor(this.state.txtColor.toLocaleLowerCase());
        this.setState({txtColor:''})
    }

    render(){
        const uri = 'https://www.w3schools.com/Colors/colors_names.asp';
        const {navigate} = this.props.navigation;
        console.log('Navigation => ' , navigate)
        return (
            <View style={styles.container}>
              <TextInput style={styles.textInput}
              placeholder="Enter a color" 
              value={this.state.txtColor}
              onChangeText={(txtColor) => this.setState({txtColor})} />
              <Text style={styles.button} onPress={this.submit}>Add</Text>
              <Text style={styles.button} onPress={()=> navigate('Web', {uri})}>Info</Text>
            </View>
        )
    }
}

ColorForm.propTypes = {
    onNewColor: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'lightgrey',
        justifyContent: 'space-around',
        height: 70,
        paddingTop: 20
    },
    textInput: {
        flex: 1,
        margin: 5,
        padding: 5,
        borderWidth: 2,
        fontSize: 20,
        borderRadius: 5,
        backgroundColor: 'snow'
    },
    button: {
        backgroundColor: 'blue',
        margin: 5,
        padding: 10,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        color:'white',
        fontSize: 20
    }
})