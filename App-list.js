import React from 'react';
import {
  StyleSheet,
  Text,
  //View, 
  ListView,

  //TouchableHighlight 
} from 'react-native';

import ColorButton from './components/colorbutton'

export default class App extends React.Component {

  constructor(props) {
    super(props)

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const availableColors = ['red', 'green', 'blue', 'salmon', 'rgba(10,34,78,0.8)', 'yellow',
    'red', 'green', 'blue', 'salmon', 'rgba(10,34,78,0.6)', 'yellow']


    this.state = {
      backgroundColor: 'blue',
      availableColors,
      datasource: this.ds.cloneWithRows(availableColors)
    }
    this.changeColor = this.changeColor.bind(this)
    this.renderRow = this.renderRow.bind(this)
  }

  changeColor(backgroundColor) {
    this.setState({ backgroundColor })
  }

  renderRow(color) {
    return (
      <ColorButton backgroundColor={color} onSelect={this.changeColor}></ColorButton>
    )
  }

  render() {
    const { backgroundColor, datasource } = this.state
    return (
      <ListView style={[styles.container, { backgroundColor }]} dataSource={datasource}
        renderRow={this.renderRow}
        renderHeader={
          () => (
            <Text style={styles.header}>Color list</Text>
          )
        }>
      </ListView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    marginBottom: 20
  },
  header: {
    fontSize: 30,
    backgroundColor: 'lightgrey',
    paddingTop: 20,
    padding: 10,
    textAlign: 'center'
  }
});
