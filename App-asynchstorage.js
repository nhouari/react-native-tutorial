import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  AsyncStorage
} from 'react-native';

import ColorButton from './components/colorbutton'
import ColorForm from './components/colorform'

export default class App extends React.Component {

  constructor(props) {
    super(props)

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const availableColors = [];


    this.state = {
      backgroundColor: 'blue',
      availableColors,
      datasource: this.ds.cloneWithRows(availableColors)
    }
    this.changeColor = this.changeColor.bind(this)
    this.renderRow = this.renderRow.bind(this)
    this.newColor = this.newColor.bind(this)
  }

  componentDidMount() {
    AsyncStorage.getItem('@ColorsListStore:Colors',
      (err, data) => {
        if (err){
          console.error('Error loading colors')
        } else {
          const availableColors = JSON.parse(data);
          this.setState({
            availableColors,
            datasource: this.ds.cloneWithRows(availableColors)
          })
        }
      }
    )
  }

  saveColors(colors) {
    AsyncStorage.setItem(
      '@ColorsListStore:Colors',
      JSON.stringify(colors)
    )
  }

  changeColor(backgroundColor) {
    this.setState({ backgroundColor })
  }

  renderRow(color) {
    return (
      <ColorButton backgroundColor={color} onSelect={this.changeColor}></ColorButton>
    )
  }

  newColor(color) {
    const availableColors = [
      ...this.state.availableColors,
      color
    ]
    this.setState({
      availableColors,
      datasource: this.ds.cloneWithRows(availableColors)
    })

    this.saveColors(availableColors);
  }

  render() {
    const { backgroundColor, datasource } = this.state
    return (

      <ListView style={[styles.container, { backgroundColor }]} dataSource={datasource}
        renderRow={this.renderRow}
        renderHeader={
          () => (
            // <Text style={styles.header}>Color list</Text>
            <ColorForm onNewColor={this.newColor} />
          )
        }>
      </ListView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    marginBottom: 20
  },
  header: {
    fontSize: 30,
    backgroundColor: 'lightgrey',
    paddingTop: 20,
    padding: 10,
    textAlign: 'center'
  }
});
