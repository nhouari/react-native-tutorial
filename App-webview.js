import React from 'react';
import {
  StyleSheet,
  ListView
} from 'react-native';

import ColorList from './components/colorlist'
import ColorInfo from './components/colorinfo'
import WebPage from './components/webpage'
import { createStackNavigator } from 'react-navigation'

export default createStackNavigator({
  Home: {
    screen: ColorList
  },
  Details: {
    screen: ColorInfo
  },
  Web: {
    screen: WebPage
  }
})
